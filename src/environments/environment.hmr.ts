import { common } from "./common_environment"
export const environment = {
  hmr: true,
  production: false,
  baseUrl: "http://localhost:8000/api/",
  common: common
};
