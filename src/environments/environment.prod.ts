import { common } from "./common_environment"
export const environment = {
  hmr: false,
  production: true,
  baseUrl: "http://107.180.107.13:8000/api/",
  common: common
};
