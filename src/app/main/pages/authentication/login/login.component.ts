import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { FuseConfigService } from "@fuse/services/config.service";
import { fuseAnimations } from "@fuse/animations";
import { HttpClient } from "@angular/common/http";
import { Router } from "@angular/router";
import { AuthService } from "../../../services/auth.service";
import { GlobalService } from "../../../services/global.service";
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';

import { AuthService as SocialAuthService } from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';

@Component({
  selector: "login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations,
})
export class LoginComponent implements OnInit {
  // auth2: any;
  // @ViewChild('ingoogle', { static: true }) img: ElementRef;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  loginForm: FormGroup;
  socialUser: SocialUser;
  process = ""

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _formBuilder: FormBuilder,
    private router: Router,
    private authSerivice: AuthService,
    private gs: GlobalService,
    private _snackBar: MatSnackBar,
    private socialAuthService: SocialAuthService
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: { hidden: true, },
        toolbar: { hidden: true, },
        footer: { hidden: true, },
        sidepanel: { hidden: true, },
      },
    };
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", Validators.required],
    });
    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
    });
  }

  signInWithGoogle(): void {
    this.process = 'sending';
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((user) => {
      console.log("google user+++++", user)
      this.socialLogin(user)
    });
  }

  signInWithFB(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((user) => {
      console.log("facebook user+++++", user)
      this.socialLogin(user)
    });
  }
  socialLogin(user: any): void {
    this.process = 'sending';
    this.authSerivice
      .socialLogin(user)
      .subscribe(
        (data) => this.loginSuccess(data),
        (error) => this.loginFailed(error)
      );
  }
  signInWithCridential(): void {
    const user: any = {
      email: this.loginForm.get("email").value,
      password: this.loginForm.get("password").value,
    };

    this.process = 'sending';
    this.authSerivice
      .getAccessToken({ user: user })
      .subscribe(
        (data) => this.loginSuccess(data),
        (error) => this.loginFailed(error)
      );
  }

  loginSuccess(data): void {
    if (data["user"]["token"]) {
      localStorage.setItem("currentuser", JSON.stringify(data["user"]));
      this.gs.setUserDetails(data["user"]);
      if (data["user"]["role"] === "admin") {
        this.router.navigate(["/admin"]);
      } else {
        this.router.navigate(["/dashboard"]);
      }
    } else {
      console.log("data", data)
    }
  }
  loginFailed(error): void {
    this.process = 'done';
    console.log("error", error);
    this._snackBar.open(error, '', {
      duration: 3000,
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
  signOut(): void {
    this.socialAuthService.signOut();
  }
}
